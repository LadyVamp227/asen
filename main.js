$(document).ready(function () {
    addTableContent()
})

function search() {
    // Declare variables
    let input, filter, table, tr, th, i;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
    th = table.getElementsByTagName("th");

// Loop through all table rows, and hide those who don't match the        search query
    for (i = 1; i < tr.length; i++) {
        tr[i].style.display = "none";
        for (let j = 0; j < th.length; j++) {
            td = tr[i].getElementsByTagName("td")[j];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
                    tr[i].style.display = "";
                    break;
                }
            }
        }
    }
}

function addTableContent() {
    let tableBody = $('table tbody');
    $.getJSON("content.json", function (data) {
        $.each(data, function (i, data) {
            tableBody.append(
                "<tr><td><a href='" + data.url + "' target='_blank'>" + data.url +
                "</a></td><td>" + data.category +
                "</td><td>" + data.comment +
                "</td></tr>");
        })
    }).fail(function () {
        console.log("An error has occurred.");
    });
}
